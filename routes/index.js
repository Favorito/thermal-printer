const express = require('express');
const router = express.Router();
const printer = require('node-thermal-printer');

function number_format (number, decimals, decPoint, thousandsSep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''

    var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k).toFixed(prec)
    }

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')

    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }

    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }

    return s.join(dec)
}

printer.init({
    type: printer.printerTypes.EPSON,
    interface: '/dev/usb/lp0',
    width: 63,
    characterSet: 'SLOVENIA',
    removeSpecialCharacters: false,
    replaceSpecialCharacters: true
});

printer.isPrinterConnected((isConnected) => {
    console.log(isConnected);
});

router.post('/nf', function(req, res, next) {
    const name = req.body.name;
    const street = req.body.street;
    const cnpj = req.body.cnpj;
    const fee = number_format((req.body.fee * 100), 2, '.', '.');
    const total = number_format(req.body.total, 2, ',', '.');
    const moneyReceived = number_format(req.body.moneyReceived, 2, ',', '.');
    const change = number_format(req.body.change, 2, ',', '.');
    const cart = typeof req.body.cart === 'object' ? req.body.cart : JSON.parse(req.body.cart);

    printer.openCashDrawer();
    printer.alignCenter();
    printer.setTextQuadArea();
    printer.println(name);

    printer.setTextNormal();
    printer.newLine();
    printer.setTypeFontA();
    printer.println(street);
    printer.println(`CNPJ ${cnpj}`);
    printer.newLine();
    printer.println('------------------------------------------------');
    printer.alignLeft();

    for (let product in cart) {
        let productName = (cart[product].name > 34) ? cart[product].name.substr(0, 31 - 1) + '...' : cart[product].name;

        let spaces = 48 - productName.length;
        for (let i = 0; i < spaces; i++) {
            productName = productName + ' ';
        }

        let productAmountAndPrice = ` ${cart[product].amount} x R$ ${number_format(cart[product].price, 2, ',', '.')}`;

        printer.println(productName.substr(0, (48 - 1) - productAmountAndPrice.length) + productAmountAndPrice);
    }

    printer.alignCenter();
    printer.println('------------------------------------------------');
    printer.newLine();

    printer.alignRight();
    printer.println(`Tarifas adicionais ${fee}%`);
    printer.println(`Valor total a pagar R$ ${total}`);
    printer.println(`Dinheiro recebido R$ ${moneyReceived}`);
    printer.println(`Troco R$ ${change}`);

    /*printer.tableCustom([
        { text: 'Tarifas adicionais', align: 'LEFT', width: 0.6 },
        { text: `${fee}%`, align: 'RIGHT', width 0.4 }
    ]);

    printer.tableCustom([
        { text: 'Total', align: 'LEFT', width: 0.6 },
        { text: `R$${total}`, align: 'RIGHT', width 0.4 }
    ]);

    printer.tableCustom([
        { text: 'Dinheiro recebido', align: 'LEFT', width: 0.6 },
        { text: `R$${moneyReceived}`, align: 'RIGHT', width 0.4 }
    ]);

    printer.tableCustom([
        { text: 'Troco', align: 'LEFT', width: 0.6 },
        { text: `R$${change}`, align: 'RIGHT', width 0.4 }
    ]);*/

    printer.alignCenter();
    printer.newLine();
    printer.newLine();
    printer.println('Agradecemo-lhes pela preferencia.');
    printer.newLine();
    printer.newLine();
    printer.println('Tarifas adicionais podem ter sido aplicadas em razao da forma de pagamento dos produtos. Consulte o operador em caso de duvidas.');

    printer.cut();
    printer.execute();

    res.status(200).json({ success: true });
});

router.post('/cupom', function(req, res, next) {
    const name = req.body.name;
    const street = req.body.street;
    const cnpj = req.body.cnpj;
    const total = req.body.total;
    const cupons = total / 10;

    if (cupons >= 1) {
        printer.openCashDrawer();
        printer.alignCenter();
        printer.setTextQuadArea();
        printer.println(name);

        printer.setTextNormal();
        printer.newLine();
        printer.setTypeFontA();
        printer.println(street);
        printer.println(`CNPJ ${cnpj}`);

        printer.newLine();
        printer.newLine();
        printer.println('Nome completo');
        printer.newLine();
        printer.println('------------------------------------------');

        printer.newLine();
        printer.println('Telefone');
        printer.newLine();
        printer.println('--------------------------------');

        printer.newLine();
        printer.newLine();
        printer.println('Este cupom lhe garante o direito de concorrer ao premio de duas cestas basicas no sorteio que sera realizado no dia 13 de maio, no dia das maes, as 11h.');
        printer.newLine();
        printer.println('Lembre-se: quanto mais cupons voce tiver, maiores sao suas chances de ganhar o premio.');
        printer.newLine();
        printer.newLine();
        printer.cut();

        for (let i = 1; i <= cupons; i++) {
            printer.execute();
        }
    }

    res.status(200).json({ success: true });
});

module.exports = router;
